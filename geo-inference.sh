$SPARK_HOME/bin/spark-submit \
    --class com.sysomos.location.core.GeoLocationFinder \--master yarn-client \
    --num-executors 85\
    --driver-memory 7g \
    --executor-memory 5g \
    target/location-engine-1.0-SNAPSHOT-jar-with-dependencies.jar -i seeds.csv -a 0.5 -g 0.6 -k 50 -o LocationInference -m 1 \
