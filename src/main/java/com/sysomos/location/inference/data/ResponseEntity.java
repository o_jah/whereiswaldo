package com.sysomos.location.inference.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.sysomos.activity.service.utils.ListUtils;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseEntity {

	public String statusCode;
	public String recordFound;
	public List<JobDetails> results;

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class JobDetails {
		public String jobId;
		public String appId;
		public String userId;
		public String requestId;
		public String status;

		public String toString() {
			return String.format("Job Id: %s, status: %s, appId: %s", jobId,
					status, appId);
		}
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("StatusCode: ").append(statusCode).append(" ");
		sb.append("JobDetails: ").append(ListUtils.join(results, ", "));

		return sb.toString();
	}
}
