package com.sysomos.location.inference.data;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.commons.lang3.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.api.java.function.VoidFunction;
import org.apache.spark.mllib.feature.HashingTF;
import org.apache.spark.mllib.feature.IDF;
import org.apache.spark.mllib.linalg.Vector;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.sysomos.location.inference.data.SolrBatchAPITwitterFeedFetcher.RequestType;
import com.sysomos.location.utils.NgramBuilder;

import scala.Tuple2;

public class FetchTweets {

	private static String APP_NAME = "GeoLocation.FetchTweets";
	private static SparkConf sparkConf;
	private static JavaSparkContext sparkContext;

	private static double MIN_TF_IDF = 0.4;

	static {
		sparkConf = new SparkConf().setAppName(APP_NAME);
		sparkContext = new JavaSparkContext(sparkConf);

		System.setProperty("org.slf4j.simpleLogger.defaultLogLevel", "trace");
	}

	private static final Logger LOG = LoggerFactory
			.getLogger(FetchTweets.class);

	public static Map<Long, Map<String, Double>> fetch(
			final ImmutableList<Long> userIds, long startDateMillis,
			long endDateMillis, Map<Long, Integer> usersNumOriginalTweets) {

		System.out.println("*******************************");
		System.out.println("FetchTweets.fetch: start " + new DateTime());

		int retweeters = 0, noContent = 0;
		Multimap<Long, String> usersTweets = ArrayListMultimap.create();

		try {
			File file = new SolrBatchAPITwitterFeedFetcher.Builder(
					RequestType.POST, userIds, startDateMillis, endDateMillis)
							.build().fetch();
			FileSentenceIterator iter = new FileSentenceIterator(file);
			while (iter.hasNext()) {
				String json = iter.nextSentence();
				TweetItem tweet = new ObjectMapper().readValue(json,
						TweetItem.class);
				System.out.println(tweet.contents);
				if (StringUtils.isEmpty(tweet.contents)
						|| tweet.contents.indexOf("RT @") >= 0) {
					retweeters++;
					continue;
				}
				usersTweets.put(Long.parseLong(tweet.actorId.replace("TT", "")),
						tweet.contents);
			}
		} catch (Throwable e) {
			LOG.error("Job failed. Couldn't get Tweets from Prod SOLR");
			throw new RuntimeException(e);
		}

		int threads = Runtime.getRuntime().availableProcessors();
		threads = threads < 10 ? 10 : threads;

		ExecutorService service = Executors.newFixedThreadPool(threads);

		Map<Long, Future<Map<String, Double>>> futures;
		futures = new HashMap<Long, Future<Map<String, Double>>>();

		for (Long userId : userIds) {
			if (!usersTweets.containsKey(userId)) {
				noContent++;
				continue;
			}
			final List<String> posts = new ArrayList<String>(
					usersTweets.get(userId));
			Callable<Map<String, Double>> callable;
			callable = new Callable<Map<String, Double>>() {
				public Map<String, Double> call() throws Exception {
					return normalize(posts);
				}
			};
			usersNumOriginalTweets.put(userId, posts.size());
			futures.put(userId, service.submit(callable));
		}
		service.shutdown();

		System.out
				.println("*** Users with no original contents: " + retweeters);
		System.out.println("*** Users with no contents at all: " + noContent);
		System.out
				.println("*** Number of threads submitted: " + futures.size());

		Map<Long, Map<String, Double>> userIdTfIdf;
		userIdTfIdf = new HashMap<Long, Map<String, Double>>();

		for (long id : userIds) {
			try {
				Future<Map<String, Double>> thread = futures.get(id);
				if (thread == null)
					continue;
				userIdTfIdf.put(id, thread.get());
			} catch (InterruptedException e) {
				String message = "FetchTweets.fetch Exception [ "
						+ e.getLocalizedMessage()
						+ " ] when fetching tweets of " + id;
				LOG.error(message, e);
			} catch (ExecutionException e) {
				String message = "FetchTweets.fetch Exception [ "
						+ e.getLocalizedMessage()
						+ " ] when fetching tweets of " + id;
				LOG.error(message, e);
			}
		}
		System.out.println("FetchTweets.fetch: end " + new DateTime());
		System.out.println("*******************************");
		sparkContext.close();
		return userIdTfIdf;

	}

	private static Map<String, Double> normalize(final List<String> contents) {
		final int ngrams = 3;
		// TODO In case of errors, do this parallelize(contents, 1)
		JavaRDD<String> lines = sparkContext.parallelize(contents);

		final HashingTF tf = new HashingTF();

		JavaRDD<WordFreq> ngramsTF = lines
				.map(new Function<String, WordFreq>() {
					private static final long serialVersionUID = -5L;

					@Override
					public WordFreq call(String s) throws Exception {
						List<String> triGrams = NgramBuilder.build(s, ngrams);
						return new WordFreq(triGrams, tf.transform(triGrams));
					}
				});
		ngramsTF.cache();

		JavaRDD<Vector> flattened = ngramsTF
				.map(new Function<WordFreq, Vector>() {
					private static final long serialVersionUID = 1L;

					@Override
					public Vector call(WordFreq arg) throws Exception {
						return arg.freq;
					}
				});
		JavaRDD<Vector> ngramsTFIDF = new IDF().fit(flattened)
				.transform(flattened);

		JavaRDD<WordFreq> wordTFIDF = ngramsTFIDF.zip(ngramsTF)
				.map(new Function<Tuple2<Vector, WordFreq>, WordFreq>() {
					private static final long serialVersionUID = 1L;

					@Override
					public WordFreq call(Tuple2<Vector, WordFreq> t)
							throws Exception {
						return new WordFreq(t._2.ngrams, t._1);
					}
				});

		JavaRDD<Tuple2<String, Double>> results = wordTFIDF.flatMap(
				new FlatMapFunction<WordFreq, Tuple2<String, Double>>() {
					private static final long serialVersionUID = 1L;

					@Override
					public Iterable<Tuple2<String, Double>> call(WordFreq arg)
							throws Exception {
						List<Tuple2<String, Double>> tuples;
						tuples = new ArrayList<Tuple2<String, Double>>();
						double[] values = arg.freq.toArray();
						for (int i = 0; i < arg.ngrams.size(); i++) {
							if (values[i] < MIN_TF_IDF)
								continue;
							Tuple2<String, Double> tuple = new Tuple2<String, Double>(
									arg.ngrams.get(i), values[i]);
							tuples.add(tuple);
						}
						return tuples;
					}
				});

		final Map<String, Double> weights = new HashMap<String, Double>();
		results.foreach(new VoidFunction<Tuple2<String, Double>>() {

			private static final long serialVersionUID = 1L;

			@Override
			public void call(Tuple2<String, Double> arg) throws Exception {
				double value = weights.containsKey(arg._1)
						? Math.max(weights.get(arg._1), arg._2) : arg._2;
				weights.put(arg._1, value);
			}
		});

		return weights;
	}

	private static class WordFreq {
		public List<String> ngrams;
		public Vector freq;

		public WordFreq(List<String> ngrams, Vector freq) {
			this.ngrams = ngrams;
			this.freq = freq;
		}
	}

}
