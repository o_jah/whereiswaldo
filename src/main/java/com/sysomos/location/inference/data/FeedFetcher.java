package com.sysomos.location.inference.data;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public interface FeedFetcher<T> {

	public T fetch() throws Throwable;

	public static class UTF8Encoder {
		public static String encode(String s) {
			try {
				return URLEncoder.encode(s, "UTF-8");
			} catch (UnsupportedEncodingException e) {
				throw new UnsupportedOperationException(e);
			}
		}
	}
}
