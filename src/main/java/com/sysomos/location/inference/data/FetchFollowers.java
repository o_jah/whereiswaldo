package com.sysomos.location.inference.data;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMultimap;
import com.google.common.collect.Multimap;
import com.sysomos.core.search.transfer.Relationship;
import com.sysomos.core.search.twitter.impl.HbaseRelationshipSearchServiceImpl;
import com.sysomos.location.exception.FetchFollowersException;
//import com.sysomos.location.exception.FilterFollowersException;

public class FetchFollowers {

	private static final Logger LOG = LoggerFactory
			.getLogger(FetchFollowers.class);

	public static ImmutableMultimap<Long, Long> fetch(
			ImmutableList<Long> seedList) throws FetchFollowersException {

		if (CollectionUtils.isEmpty(seedList)) {
			throw new FetchFollowersException("Seed list is null or empty");
		}
		System.out.println("*******************************");
		System.out.println("FetchFollowers.fetch: start " + new DateTime());

		Multimap<Long, Long> followers = ArrayListMultimap.create();
		try {
			HbaseRelationshipSearchServiceImpl service;
			service = new HbaseRelationshipSearchServiceImpl();
			Map<Long, Collection<Long>> results = service
					.get(Relationship.FOLLOWERS, seedList);

			for (Map.Entry<Long, Collection<Long>> entry : results.entrySet()) {
				followers.putAll(entry.getKey(), entry.getValue());
			}
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new FetchFollowersException(e);
		}
		System.out.println("FetchFollowers.fetch: end " + new DateTime());
		return ImmutableMultimap.copyOf(followers);
	}
}
