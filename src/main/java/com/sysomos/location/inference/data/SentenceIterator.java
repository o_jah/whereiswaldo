
package com.sysomos.location.inference.data;

/**
 * A sentence iterator that knows how to iterate over sentence. This can be used
 * in conjunction with more advanced NLP techniques to clearly separate
 * sentences out, or be simpler when as much complexity is not needed.
 * 
 *
 */
public interface SentenceIterator {

	/**
	 * Gets the next sentence or null if there's nothing left (Do yourself a
	 * favor and check hasNext() )
	 * 
	 * @return the next sentence in the iterator
	 */
	String nextSentence();

	/**
	 * Same idea as {@link java.util.Iterator}
	 * 
	 * @return whether there's anymore sentences left
	 */
	boolean hasNext();

	/**
	 * Resets the iterator to the beginning
	 */
	void reset();

	/**
	 * Allows for any finishing (closing of input streams or the like)
	 */
	void finish();

}
