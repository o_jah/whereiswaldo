package com.sysomos.location.inference.data;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Multimap;
import com.sysomos.core.search.transfer.Relationship;
import com.sysomos.core.search.twitter.impl.HbaseRelationshipSearchServiceImpl;
import com.sysomos.location.exception.FetchFriendsException;
import com.sysomos.location.utils.SamplingUtils;

public class FetchFriends {

	private static final Logger LOG = LoggerFactory
			.getLogger(FetchFriends.class);

	public static Multimap<Long, Long> fetch(ImmutableList<Long> seedList,
			int maxFriends) throws FetchFriendsException {

		if (CollectionUtils.isEmpty(seedList)) {
			throw new FetchFriendsException("Seed list is null or empty");
		}
		System.out.println("*******************************");
		System.out.println("FetchFriends.fetch: start " + new DateTime());

		Multimap<Long, Long> friends = ArrayListMultimap.create();
		try {
			HbaseRelationshipSearchServiceImpl service;
			service = new HbaseRelationshipSearchServiceImpl();
			Map<Long, Collection<Long>> results = service
					.get(Relationship.FRIENDS, seedList);

			for (Map.Entry<Long, Collection<Long>> entry : results.entrySet()) {
				if (entry.getValue().size() > maxFriends) {
					friends.putAll(entry.getKey(), SamplingUtils.reservoir(
							new ArrayList<Long>(entry.getValue()), maxFriends));
				} else {
					friends.putAll(entry.getKey(), entry.getValue());
				}
			}
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new FetchFriendsException(e);
		}
		System.out.println("*** Friends size: [ " + friends.size() + " ]");
		System.out.println("FetchFriends.fetch: end " + new DateTime());
		return friends;
	}
}
