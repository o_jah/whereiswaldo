package com.sysomos.location.inference.data;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class TweetItem {

	public String id; // Tweet ID e.g. TT668269878328090624
	public String dataSource;
	public String contents;
	public String contentType;
	public String postType;
	public long createDate;
	public String permaLink;
	public long docId;
	public String actorId; // e.g. TT807095
	public String actorScreenName;
	public String actorName;
	public String actorIconUrl;
	public String postSource;
	public String lang;
	public int influencerScore;
	public int profanityScore;
	public String countryCode;
	public List<String> urls;
	public long followerCount;
	public long friendCount;
	public long favoriteCount;
	public double relevancyScore;
	public String city;
	public String province;
	public String country;
	public DsProperties dsProperties;
	public String timeZone;
	public long postCount;
	public boolean actorVerified;

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class DsProperties {
		public String actorProfileUrl;
	}
}
