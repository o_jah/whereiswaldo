package com.sysomos.location.inference.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function;
import org.apache.spark.rdd.JdbcRDD;
import org.joda.time.DateTime;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.sysomos.location.exception.FilterFollowersException;
import com.sysomos.location.utils.DBConnection;
import com.sysomos.location.utils.MapResult;

import scala.reflect.ClassManifestFactory$;

public class FilterFollowers {

	private static String app_name = "Geo Location:Filtering Program";
	private static final String CONFIG_FILE = "config.properties";
	private static final Logger LOG = LoggerFactory
			.getLogger(FilterFollowers.class);

	public static Multimap<Long, Long> filter(Multimap<Long, Long> followers)
			throws FilterFollowersException {
		if (followers == null || followers.size() == 0)
			return followers;
		System.out.println("*******************************");
		System.out
				.println("*** FilterFollowers.filter: start " + new DateTime());

		Multimap<Long, Long> filtered = ArrayListMultimap.create();

		SparkConf sparkConf = new SparkConf().setAppName(app_name);
		JavaSparkContext ctx = new JavaSparkContext(sparkConf);
		List<Long> knownLocals = null;

		try {
			knownLocals = fetchUsersWithInferredLocation(ctx);
		} catch (IOException e) {
			LOG.error(e.getLocalizedMessage(), e);
			throw new FilterFollowersException(e);
		}

		if (knownLocals == null)
			throw new FilterFollowersException("Couldn't find DB config file.");
		JavaRDD<Long> toIgnoreRdd = ctx.parallelize(knownLocals);

		if (knownLocals.size() > 0) {
			for (long userId : followers.keySet()) {
				List<Long> ids = new ArrayList<Long>(followers.get(userId));
				JavaRDD<Long> idsRdd = ctx.parallelize(ids);
				List<Long> diff = idsRdd.subtract(toIgnoreRdd).collect();
				if (CollectionUtils.isEmpty(diff))
					continue;
				filtered.putAll(userId, diff);
			}
		}
		System.out
				.println(
						"*** Followers size: [ Before: " + followers.size()
								+ ", After: " + (knownLocals.size() > 0
										? filtered.size() : followers.size())
				+ " ]");
		System.out.println("*** FilterFollowers.filter: end " + new DateTime());
		ctx.close();
		return knownLocals.size() > 0 ? filtered : followers;
	}

	private static List<Long> fetchUsersWithInferredLocation(
			final JavaSparkContext ctx) throws IOException {

		Properties prop = new Properties();

		InputStream inputStream = FilterFollowers.class.getClassLoader()
				.getResourceAsStream(CONFIG_FILE);

		prop.load(inputStream);

		String user = prop.getProperty("user");
		String pwd = prop.getProperty("password");
		String dbUrl = prop.getProperty("url2");
		String driver = prop.getProperty("driverClass");
		String table_name = "user_location";

		long lastUpdated = new DateTime().getMillis();

		JdbcRDD<Object[]> jdbc_rdd = new JdbcRDD<Object[]>(ctx.sc(),
				new DBConnection(driver, dbUrl, user, pwd),
				"select id from " + table_name
						+ " where id >= ? and id <= ? and flag = 'I' and last_updated < "
						+ String.valueOf(lastUpdated),
				0, 4000000000L, 10, new MapResult(),
				ClassManifestFactory$.MODULE$.fromClass(Object[].class));

		JavaRDD<Object[]> rdd = JavaRDD.fromRDD(jdbc_rdd,
				ClassManifestFactory$.MODULE$.fromClass(Object[].class));

		List<Long> ids = new ArrayList<Long>();
		if (rdd == null) {
			return ids;
		}
		ids = rdd.map(new Function<Object[], Long>() {
			private static final long serialVersionUID = 1L;

			@Override
			public Long call(final Object[] record) throws Exception {
				return (Long) record[0];
			}

		}).collect();

		return ids;
	}

}
