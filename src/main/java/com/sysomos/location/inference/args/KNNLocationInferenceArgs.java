package com.sysomos.location.inference.args;

import java.util.Map;

import com.sysomos.location.core.UserLocation;
import com.sysomos.location.utils.Pair;

public class KNNLocationInferenceArgs extends LocationInferenceArgs {

	public final int numNearestNeighbors;
	public final Map<Long, UserLocation> seeds;
	public final Map<Pair<Long, Long>, Double> proximity;

	public KNNLocationInferenceArgs(final Map<Long, UserLocation> seeds,
			final Map<Pair<Long, Long>, Double> proximity,
			int numNearestNeighbors) {
		this.seeds = seeds;
		this.proximity = proximity;
		this.numNearestNeighbors = numNearestNeighbors;
	}
}
