package com.sysomos.location.utils;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;

public class StatisticsUtils {

	public static <T> void meanCentered(Map<T, Double> w1, Map<T, Double> w2) {
		Set<T> keys = new HashSet<T>();

		if (!MapUtils.isEmpty(w1)) {
			keys.addAll(w1.keySet());
		}
		if (!MapUtils.isEmpty(w2)) {
			keys.addAll(w2.keySet());
		}

		for (T aKey : keys) {
			double count1 = 0, count2 = 0;
			if (w1 != null && w1.containsKey(aKey)) {
				count1 = w1.get(aKey);
			}
			if (w2 != null && w2.containsKey(aKey)) {
				count2 = w2.get(aKey);
			}
			double mean = (count1 + count2) / 2.0;
			if (w1 != null && w1.containsKey(aKey)) {
				w1.put(aKey, (count1 - mean));
			}
			if (w2 != null && w2.containsKey(aKey)) {
				w2.put(aKey, (count2 - mean));
			}
		}
	}

	public static <T> Map<T, Double> mean(Map<T, Double> w1, Map<T, Double> w2) {
		Set<T> keys = new HashSet<T>();

		if (!MapUtils.isEmpty(w1)) {
			keys.addAll(w1.keySet());
		}
		if (!MapUtils.isEmpty(w2)) {
			keys.addAll(w2.keySet());
		}

		Map<T, Double> meanMap = new HashMap<T, Double>();

		for (T aKey : keys) {
			double count1 = 0, count2 = 0;
			if (w1 != null && w1.containsKey(aKey)) {
				count1 = w1.get(aKey);
			}
			if (w2 != null && w2.containsKey(aKey)) {
				count2 = w2.get(aKey);
			}
			double mean = (count1 + count2) / 2.0;
			meanMap.put(aKey, mean);
		}
		return meanMap;
	}

}
