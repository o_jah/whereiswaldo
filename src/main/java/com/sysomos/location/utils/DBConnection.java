package com.sysomos.location.utils;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import scala.runtime.AbstractFunction0;

/**
 * Gets database values.
 * 
 * @author hminooei
 *
 */
public class DBConnection extends AbstractFunction0<Connection> implements
		Serializable {
	private static final long serialVersionUID = 4565460143895553800L;
	private String driverClassName;
	private String connectionUrl;
	private String userName;
	private String password;

	private static final Logger LOG = LoggerFactory
			.getLogger(DBConnection.class);

	public DBConnection(String driverClassName, String connectionUrl,
			String userName, String password) {
		this.driverClassName = driverClassName;
		this.connectionUrl = connectionUrl;
		this.userName = userName;
		this.password = password;
	}

	@Override
	public Connection apply() {
		try {
			LOG.info("Get driver..");
			Class.forName(driverClassName);
		} catch (ClassNotFoundException e) {
			LOG.error("Failed to load driver class", e);
		}

		Connection connection = null;
		try {
			LOG.info("Get connection..");
			connection = DriverManager.getConnection(connectionUrl, userName,
					password);
		} catch (SQLException e) {
			LOG.error("Connection failed", e);
		}

		return connection;
	}
}