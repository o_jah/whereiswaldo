package com.sysomos.location.utils;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.MapUtils;

public class WordSequenceDistance {

	@SuppressWarnings("unchecked")
	public static double getDistance(String str1, String str2) {

		Map<String, Double> featureMap1 = new HashMap<String, Double>();
		featureMap1.putAll((Map<String, Double>) ContentFeatureExtractor
				.getFeatureMap(str1));
		Map<String, Double> featureMap2 = new HashMap<String, Double>();
		featureMap2.putAll((Map<String, Double>) ContentFeatureExtractor
				.getFeatureMap(str2));

		return getDistance(featureMap1, featureMap2);
	}

	public static double getDistance(Map<String, Double> w1,
			Map<String, Double> w2) {

		if (MapUtils.isEmpty(w1) || MapUtils.isEmpty(w2)) {
			return 0;
		}

		Set<String> set = w1.keySet();
		String[] array1 = set.toArray(new String[set.size()]);
		Arrays.sort(array1);

		set = w2.keySet();
		String[] array2 = set.toArray(new String[set.size()]);
		Arrays.sort(array2);

		if (array1 == null || array2 == null) {
			return 0;
		}

		// StatisticsUtils.meanCentered(w1, w2);

		int i = 0, j = 0;
		double distance = 0;

		while (i < array1.length || j < array2.length) {
			if (j >= array2.length
					|| ((i < array1.length) && array1[i].compareTo(array2[j]) < 0)) {
				i++;
			} else if (i >= array1.length
					|| ((j < array2.length) && array1[i].compareTo(array2[j]) > 0)) {
				j++;
			} else {
				distance += Math.pow(
						(w1.get(array1[i]).doubleValue() - w2.get(array2[j])
								.doubleValue()), 2);
				i++;
				j++;
			}
		}
		return Math.sqrt(distance);
	}
}
