package com.sysomos.location.utils;

import gnu.trove.map.hash.TObjectFloatHashMap;
import gnu.trove.map.hash.TObjectIntHashMap;
import gnu.trove.set.hash.TIntHashSet;

import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Simple counter that tracks Object->count pairs in a map. Key objects are of
 * type K, and values are integers.
 */
public class Counter<K> {

	/**
	 * UTF8 charset, the application-wide default.
	 */
	public static final Charset UTF8 = Charset.forName("UTF-8");

	public static <T> Counter<T> fromMap(Map<T, Integer> m) {
		Counter<T> c = new Counter<T>();

		for (Map.Entry<T, Integer> e : m.entrySet()) {
			c.add(e.getKey(), e.getValue());
		}

		return c;
	}

	TObjectIntHashMap<K> counts;

	public Counter() {
		counts = new TObjectIntHashMap<K>();
	}

	/**
	 * Increment the count of the key by 1. If the key is not in the counter,
	 * add it with count 1.
	 */
	public void add(K key) {
		add(key, 1);
	}

	/**
	 * Increment the count of the key by specified count. If the key is not in
	 * the counter, add it with specified count.
	 */
	public void add(K key, int count) {
		counts.adjustOrPutValue(key, count, count);
	}

	public boolean containsKey(K key) {
		return counts.containsKey(key);
	}

	/**
	 * Returns the count for the given key, and zero if key is not present in
	 * this counter.
	 */
	public int getCount(K key) {
		// if no key is found in the map, the trove implementation returns zero;
		// so no need for an extra check of containsKey
		return counts.get(key);
	}

	/**
	 * Basically normalize all counts such that the sum over all keys is 1.
	 */
	@SuppressWarnings("unchecked")
	public TObjectFloatHashMap<K> getDistributionHistogram() {
		TObjectFloatHashMap<K> hist = new TObjectFloatHashMap<K>();
		int totalCount = getTotalCount();
		for (Object key : counts.keys()) {
			float p = ((float) counts.get((K) key)) / ((float) totalCount);
			hist.put((K) key, p);
		}
		return hist;
	}

	public int getKeyCount() {
		return counts.size();
	}

	/**
	 * Count of highest scoring element
	 */
	public int getMaxCount() {
		int max = 0;
		for (int val : counts.values()) {
			if (val > max) {
				max = val;
			}
		}
		return max;
	}

	public TObjectFloatHashMap<String> getPieChartData(float minPerct) {
		return getPieChartData(minPerct, 0, true);
	}

	public TObjectFloatHashMap<String> getPieChartData(float minPerct,
			boolean includePerctInLabels) {
		return getPieChartData(minPerct, 0, includePerctInLabels);
	}

	public TObjectFloatHashMap<String> getPieChartData(float minPerct,
			int maxSize, boolean includePerctInLabels) {
		return getPieChartData(minPerct, maxSize, includePerctInLabels, true);

	}

	/**
	 * @param minPerct
	 * @param maxSize
	 * @param includePerctInLabels
	 * @param includeOtherCategory
	 *            If true, include "other" category to aggregate percentage data
	 *            to 100%. If false, it does not -- used by hb csv.jsp to
	 *            retrieve chart data that matches what's displayed on GUI to
	 *            the user which does not include an "other" category.
	 * @return TObjectFloatHashMap
	 */
	@SuppressWarnings("unchecked")
	public TObjectFloatHashMap<String> getPieChartData(float minPerct,
			int maxSize, boolean includePerctInLabels,
			boolean includeOtherCategory) {
		TObjectFloatHashMap<K> hist = this.getDistributionHistogram();
		TObjectFloatHashMap<String> out = new TObjectFloatHashMap<String>();
		float otherPerct = 0;
		float totalPerct = 0;
		for (Object histKey : getTop(maxSize)) {
			float perct = (((int) (hist.get((K) histKey) * 1000)) / 10.0f);
			if (perct >= minPerct) {
				totalPerct += perct;
				if (includePerctInLabels) {
					out.put(histKey + " (" + perct + "%)", perct);
				} else {
					out.put("" + histKey, perct);
				}
			}
		}
		if (includeOtherCategory) {
			if (totalPerct <= 99) {
				otherPerct = 100 - totalPerct;
				otherPerct = ((int) (10 * otherPerct)) / 10.0f;
				if (includePerctInLabels) {
					out.put("other" + " (" + otherPerct + "%)", otherPerct);
				} else {
					out.put("other", otherPerct);
				}
			}
		}
		return out;
	}

	/**
	 * Returns a list of top elements in the counter. Size of returned list is
	 * minimum of the number of distinct objects in the counter, and the
	 * parameter maxSize. Returned list contains keys sorted in descending order
	 * of counts.
	 */
	public List<K> getTop(int maxSize) {
		if (maxSize <= 0) {
			maxSize = counts.size();
		}
		List<K> list = sortMapByValue(counts, maxSize, true);
		return list;
	}

	/**
	 * Sort the map according to the value column. Returns a list of top
	 * corresponding keys. Values can be duplicated in the map.
	 * 
	 * @param descendingOrder
	 *            if true, the map is sorted in descending order
	 */
	@SuppressWarnings("unchecked")
	public static <K> List<K> sortMapByValue(TObjectIntHashMap<K> map,
			int maxSize, boolean descendingOrder) {
		List<K> slist = new ArrayList<K>(map.size());
		TIntHashSet valSet = new TIntHashSet();
		valSet.addAll(map.values());
		int[] valList = valSet.toArray();
		Arrays.sort(valList);
		if (descendingOrder) {
			reverse(valList);
		}
		int count = 0;
		for (int v : valList) {
			for (Object kObj : map.keys()) {
				K k = (K) kObj;
				if (map.get(k) == v) {
					slist.add(k);
					count++;
					if (maxSize > 0 && count >= maxSize) {
						return slist;
					}
				}
			}
		}
		return slist;
	}

	/**
	 * Reverse the order of elements in an array.
	 */
	public static void reverse(int[] b) {
		int left = 0;
		int right = b.length - 1;

		while (left < right) {
			// exchange the left and right elements
			int temp = b[left];
			b[left] = b[right];
			b[right] = temp;
			// move the bounds toward the center
			left++;
			right--;
		}
	}

	public List<Pair<K, Integer>> getTopWithCounts(int maxSize) {
		if (maxSize <= 0) {
			maxSize = counts.size();
		}
		List<K> list = getTop(maxSize);
		List<Pair<K, Integer>> lc = new ArrayList<Pair<K, Integer>>(list.size());
		for (int i = 0; i < list.size(); i++) {
			K key = list.get(i);
			int value = counts.get(key);
			lc.add(new Pair<K, Integer>(key, value));
		}
		return lc;
	}

	public List<K> getTopSortAphabetically(int maxSize) {
		List<K> list = getTop(maxSize);
		Collections.sort(list, new Comparator<K>() {
			public int compare(K o1, K o2) {
				return ("" + o1).compareTo("" + o2);
			}
		});
		return list;
	}

	/**
	 * Total count, summed over all key objects in this data-structure.
	 */
	@SuppressWarnings("unchecked")
	public int getTotalCount() {
		int count = 0;
		for (Object key : counts.keys()) {
			count += counts.get((K) key);
		}
		return count;
	}

	public Object[] keys() {
		return counts.keys();
	}

	/**
	 * Returns a set of keys. Since Java does not allow creation of generic
	 * arrays, please provide a zero length array of type K as an argument.
	 */
	public K[] keys(K[] keyArray) {
		return counts.keys(keyArray);
	}

	/**
	 * Returns all keys, sorted by counts.
	 */
	public List<K> keysSorted() {
		return getTop(counts.size());
	}

	public void removeKey(K key) {
		counts.remove(key);
	}

}
