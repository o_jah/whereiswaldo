package com.sysomos.location.exception;

public class GeoSocialProximityException extends GeoLocationFinderException {

	private static final long serialVersionUID = -6611428769319094677L;

	public GeoSocialProximityException(String message) {
		super(message);
	}

	public GeoSocialProximityException(Throwable cause) {
		super(cause);
	}

	public GeoSocialProximityException(Exception e) {
		super(e);
	}
}
