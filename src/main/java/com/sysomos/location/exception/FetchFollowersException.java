package com.sysomos.location.exception;

public class FetchFollowersException extends GeoLocationFinderException {

	private static final long serialVersionUID = -6611428769319094677L;

	public FetchFollowersException(String message) {
		super(message);
	}

	public FetchFollowersException(Throwable cause) {
		super(cause);
	}

	public FetchFollowersException(Exception e) {
		super(e);
	}
}
