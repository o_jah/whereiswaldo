package com.sysomos.location.exception;

public class FilterFollowersException extends GeoLocationFinderException {

	private static final long serialVersionUID = -6611428769319094677L;

	public FilterFollowersException(String message) {
		super(message);
	}

	public FilterFollowersException(Throwable cause) {
		super(cause);
	}

	public FilterFollowersException(Exception e) {
		super(e);
	}
}
