package com.sysomos.location.core;

public class UserLocation {

	public Float latitude = null;
	public Float longitude = null;
	public String location = null;

	private static final char SEP = 0x1f;

	public UserLocation(float latitude, float longitude, String location) {
		this.latitude = latitude;
		this.longitude = longitude;
		this.location = location;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		String lat = latitude == null ? "" : String.valueOf(latitude);
		builder.append(SEP).append(lat).append(SEP).append("\t");
		String llong = longitude == null ? "" : String.valueOf(longitude);
		builder.append(SEP).append(llong).append(SEP).append("\t").append(SEP)
				.append(location).append(SEP);
		return builder.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((latitude == null) ? 0 : latitude.hashCode());
		result = prime * result
				+ ((location == null) ? 0 : location.hashCode());
		result = prime * result
				+ ((longitude == null) ? 0 : longitude.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserLocation other = (UserLocation) obj;
		if (latitude == null) {
			if (other.latitude != null)
				return false;
		} else if (!latitude.equals(other.latitude))
			return false;
		if (location == null) {
			if (other.location != null)
				return false;
		} else if (!location.equals(other.location))
			return false;
		if (longitude == null) {
			if (other.longitude != null)
				return false;
		} else if (!longitude.equals(other.longitude))
			return false;
		return true;
	}

}
