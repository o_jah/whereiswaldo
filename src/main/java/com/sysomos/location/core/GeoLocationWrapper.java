package com.sysomos.location.core;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

public class GeoLocationWrapper {

	private static final String URL = " https://twitter.com/intent/user?user_id=";

	public static Map<Long, UserLocation> decorate(
			Map<Long, UserLocation> locations) {
		if (MapUtils.isEmpty(locations))
			return locations;
		Map<Long, UserLocation> results = new HashMap<Long, UserLocation>();
		for (Map.Entry<Long, UserLocation> entry : locations.entrySet()) {
			long userId = entry.getKey();
			UserLocation userLocal = entry.getValue();
			if (userLocal == null)
				continue;
			UserLocationDecorator instance = new UserLocationDecorator(userId,
					userLocal.latitude, userLocal.longitude, userLocal.location);
			results.put(userId, instance);
		}
		return results;
	}

	public static class UserLocationDecorator extends UserLocation {
		private String profileLink = null;

		public UserLocationDecorator(long userId, float latitude,
				float longitude, String location) {
			super(latitude, longitude, location);
			this.profileLink = URL + userId;
		}

		public String toString() {
			StringBuilder builder = new StringBuilder(super.toString());
			builder.append("\t").append("TTP").append(profileLink);
			return builder.toString();
		}
	}
}
